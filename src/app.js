const express = require('express');
const app = express();

app.get('/status', (req, res) => {
  res.status(200).send();
  res.end();
});

app.get('/cities', (req, res) => {
  const cities = ['Amsterdam', 'Berlin', 'New York', 'San Francisco', 'Tokyo'];
  res.json(cities);
  res.end();
})

app.use((req, res) => {
  res.status(404).send('Unable to find the requested resource!');
  res.end();
});

const port = process.env.PORT || 3000;
app.listen(port);
